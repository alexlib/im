
"""
Run this and then run the batch file:'associate.bat' with administrator
privilges
"""
import subprocess
import os
import sys

import tempfile
import sys
import io
d = sys.path.pop(0)
try:
    # look for an installed library first
    import IM
except BaseException:
    sys.path.insert(0, d)
    import IM

# python executable
target = sys.executable
base, extn = os.path.splitext(sys.executable)

if True:
    # hidden console
    target = ''.join((base + 'w', extn))

# loading script - if this fails you can put in the location manually
script = os.path.abspath('loader.py')
##script = os.path.abspath('vector_loader.py')
if not os.path.isfile(script):
    print('\a unable to determine location of "loader.py"')

# extensions to associate
extns = ['.pkl', '.im7', '.vector', '.image', '.VC7', '.IM7', '.h5']
lines = ['assoc {0}=Python.IMvector'.format(extn) for extn in extns]

# associate with extensions
lines.append('ftype Python.IMvector="{0}" "{1}" "%%1"'.format(target, script))
lines.append('pause')

try:
    f = open('associate.bat', 'w')
##    f = io.open('associate.bat', 'w')
    for line in lines:
        f.write(line + '\n')

finally:
    f.close()
