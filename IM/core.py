from __future__ import division, absolute_import, print_function
#-------------------------------------------------------------------------------
# Name:        core.py
# Purpose:     Interface module to DaVis filetypes IM7 and VC7
# IM7 and VC7 objects are containers for arrays of data with the shape (frame, ny, nx)
# where ny is the number of rows and nx is the number of columns
# Author:      Alan Fleming
#
# Created:     17/06/2010
# Copyright:   (c) Alan 2010, 2011, 2012
# Licence:     ShareAlike
#-------------------------------------------------------------------------------
#!/usr/bin/env python

__metaclass__ = type

try:
    import ReadIM
    from ReadIM import BufferTypeAlt
except ImportError:
    import warnings
    mesg = 'Module ReadIM is not installed.'\
            ' It will not be possible to read IM file types'
    warnings.warn(mesg)

from ._np_methods import npMethods
from . import davis
from . import common
from .common import Storage, iterflatten, Bunch, BunchMappable, \
                   __storage_init__, __storage_new__, to_gif_func, calc_once
import os, re, ctypes
import numpy as np
from PIL import Image, ImageDraw
from matplotlib.widgets import Slider
from pylab import plt

import weakref
import copy
import io
import sys


__all__ = ['IM7', 'VC7', 'VC7u', 'VecBrowser', 'IMBrowser','loader', 'demo_VC7',
            'demo_IM7', 'BufferTypeAlt']

def map_D(D, a, b, dtype=float):
    """ Generic mapping from coordinates to pixel position
    using slope a intersect b
    returns (D - b)/a
    """
    return np.array(((D - b) / a), dtype=dtype)

def map_d(d, a, b, dtype=float):
    """ Generic mappining from pixel position to coordinates
        using slope a intersect b
        returns d * a + b
    """
    return np.array((a * d + b), dtype=dtype)

def new_entry(dest_dict, prefix):
        counter = 0
        name = prefix
        while name in dest_dict:
            counter += 1
            name = prefix + '_%02d' % counter
        return name

# Function to extract number
def get_num(line):
    newstr = ''.join((ch if ch in '0123456789.-' else ' ') for ch in line)
    listOfNumbers = [float(i) for i in newstr.split()]
    
    return listOfNumbers

# classes
#-------------------------------------------------------------------------------


class EvalFunction():
    def __init__(self, fstr, extern_dict=None):

        if extern_dict is None:
            extern_dict = {}
        self._extern_dict = extern_dict

        if not fstr is None:
            if type(fstr) is not str:
                raise TypeError('you must pass a string not a:%s' % type(fstr))
            s = fstr.find('(')
            extern_dict['name']   = fstr[:s]
            extern_dict['string'] = fstr
            extern_dict['active'] = True

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.string

    @property
    def active(self):
        return self._extern_dict['active']
    @active.setter
    def active(self, state):
        self._extern_dict['active'] = bool(state)

    @property
    def name(self):
        return self._extern_dict['name']

    @property
    def string(self):
        return self._extern_dict['string']


    @property
    def post_functions(self):
        if not hasattr(self._extern_dict, 'post_functions'):
            self._extern_dict['post_functions'] = []
        return self._extern_dict['post_functions']
    @post_functions.setter
    def post_functions(self, s):
        for ss in iterflatten(s):
            if ss not in self.post_functions:
                self.post_functions.append(str(ss))


class PltCustom(Storage):
    """ Default show objects
    """
    _plt_function_names = [obj for obj in dir(plt)
                 if (type(eval('plt.'+obj)) == type(plt.plot))&(obj[0]!='_')]


    def __init__(self, name, **kwargs):

        super(PltCustom, self).__init__(name, **kwargs)

#-------------------------------------------------------------------------------
#properties

    @property
    def plt_function_names(self):
        return self._plt_function_names

    @property
    def plt_fig_args(self):
        return self.data.get('plt_fig_args')
    @plt_fig_args.setter
    def plt_fig_args(self,item):
        key,value = item
        self._data['plt_fig_args'][key] = value

    @property
    def plt_args(self):
        if not hasattr(self, '_plt_args'):
            self._plt_args = {}
        return self._plt_args
    @plt_args.setter
    def plt_args(self, item):
        key, val = item
        self.plt_args[key] = val


    @property
    def plt_fig_args(self):
        if not hasattr(self, '_plt_fig_args'):
            self._plt_fig_args = {}
        return self._plt_fig_args

    @property
    def plt_functions(self):
        if not hasattr(self, '_plt_functions'):
            self._plt_functions = {}

        fcns = self._plt_functions
        fs = {}

        for name, settings in list(fcns.items()):
            assert name == settings['name']
            fcn = EvalFunction(None, settings)
            fs[name]=fcn
        return BunchMappable(fs, True)


    @plt_functions.setter
    def plt_functions(self, item):
        if not hasattr(self, '_plt_functions'):
            self._plt_functions = {}

        fcns = self._plt_functions

        function = EvalFunction(item, {})
        fcns[function.name] = function._extern_dict



#-------------------------------------------------------------------------------
#methods
    def _plt_functions_eval_all(self, fig=None, mappable=None,
                            ax=None, cax=None):
        """
        Evaluate all plt_functions as stored in self.plt_functions
        Append more objects as necessary
        f = figure
        mappable = Q, or A
        ax = Axes
        cax = an axis to stick the colorbar
        p = plot
        __dict__ should be the dictionary of the child object, to allow for access to objects such as self.I etc.
        access is via s. rather than the reserved self.
        """
##        fig = plt.figure(fig.number)
        self.plt_functions # initialise for none
        plt.sca(ax)

        fcs = self.plt_functions

        for name in fcs:
            funct = getattr(fcs,name)
            if funct.active:
                try:
                    res = eval(funct.string)
                except:
                    print('cannot evaluate plt_function.',funct.name)
                    print(funct.string)
                if funct.post_functions:
                    for s in funct.post_functions:
                        eval('res.' + s)


class IM(PltCustom, npMethods):
    """ A class to to load DaVis files, if no name is passed then a new
        buffer is created if the buffer_args is provided: the simplest way to do
        this is to create a new buffer in memory
        buffer attributes: (b, b.nx ,b.ny, b.nz, b.nf,
                            b.isFloat, b.vectorGrid, b.imageSubType)
    """
    mask_coordinates = {}
    _active_masks = set()
    _type = 'im'
    _extensions = ['.vc7', '.VC7','.IM7', '.im7']

    @__storage_new__
    def __new__(cls, name, buff=None, force_reload=False, **kwargs):
        inst = super(IM, cls).__new__(cls, name, force_reload=force_reload, **kwargs)

        if buff is None and not inst.loadfile:
                    raise IOError('Insufficient information to load (file not found)'\
                                ' or create (no buffer provided) {0}'.format(inst))

        return inst

    @__storage_init__
    def __init__(self, name, buff,
                    force_reload=False, load=False, **kwargs):

        self._slicer = IMSlicer(self)
        self.__applied_masks__  = set([None])
        self._dtype = kwargs.get('dtype', 'float')

        if not (self.loadfile or buff):
            if not buff:
                for member in self.members_family:
                    if member.name == self.name and issubclass(self.__class__,
                                                             member.__class__):
                        if member.loaded:
                            self.__reset__()
                            self.data.update(member.data)
                            return

                        elif member.loadfile:
                            self.loadfile = member.loadfile
                            break
                    else:
                        continue
                    raise IOError('File not found:\n%s' % name)
            else:
                raise IOError('insufficient information to create %s' % repr(self))

        super(IM, self).__init__(None, force_reload=force_reload, **kwargs)

        if buff:
            buff    = BufferTypeAlt(buff)
            if 'frames' in kwargs:
                buff.nf = kwargs['frames']
            self._set_buffer(buff, False)
        return buff

    def __reset__(self):
        super(IM,self).__reset__()
        self._data['attributes']    = {}

    def __iter__(self):
        """
        Reveal the a base iteratively, returns
        """
        axes = self.axes
        for f in range(self.frames):
            arr = [ax[f] for ax in axes]
            yield np.array(arr)

    def __nonzero__(self):

        """
        Truth test for object applictions include:
        if obj:
            ...
        if not obj:
            ...

        Returns
        -------
        nonzero: bool
            Whether the object should be considered to exist.
        """

        res = False

        if self.loaded:
            for ax in self.axes:
                res = res | ax.data.any()
        elif self.loadfile:
            res = True

        return bool(res)

    def __bool__(self):
        # python3 version
        return self.__nonzero__()


    def __getitem__(self, a):

#-------------------------------------------------------------------------------
# handle different types of arguments


        if isinstance(a, slice):

            shape = ax=self.axes[0][a].shape
            frames = shape[0]

        elif isinstance(a, int):
            if a >= self.frames:
                msg = 'index {0} is out of bounds for frame '\
                    'with size {1} '.format(a, self.frames)
                raise IndexError( msg)
            frames = 1
            shape = tuple([frames]+list(self.shape[1:]))
            a = slice(a,a+1)

        elif (isinstance(a, np.ndarray) and len(a) == a.size == self.frames):
        # assume a is a boolean type array
            a = list(a.nonzero()[0])
            frames = len(a)
            if any(np.array(a)>=self.frames):
                msg ='index(s) {0} out of bounds for frame with size {1}'.format(
                            a, self.frames)
                raise IndexError(msg)

            shape = tuple([frames]+list(self.shape[1:]))

        else:
            # assume a list type of object
            frames = len(a)
            if any(np.array(a)>=self.frames):
                msg ='index(s) {0} out of bounds for frame with size {1}'.format(
                            a, self.frames)
                raise IndexError(msg)

            shape = tuple([frames]+list(self.shape[1:]))
            frames = len(a)

#-------------------------------------------------------------------------------
        buff = ReadIM.newBuffer(self.window,shape[2],shape[1],
                    self.buffer.vectorGrid, self.buffer.image_sub_type,
                    frames=frames,
                    scaleIoffset  = self.buffer.scaleI.offset,
                    scaleIfactor = self.buffer.scaleI.factor)

        name, root = self.get_new_name('sl_{0}'.format(str(a)),append_name=True)

        # build a new target object for the slice
        im = self.__class__(name, buff=buff, family=self.family, force_new=True)

        for b, c in zip(im.axes, self.axes):
            b[:] = c[a]

        im.attributes.update(self.attributes)
        im.data['store'].update(self.data['store'])

        return im


    def __setitem__(self, index, a):
        """
        """

        if isinstance(a, IM):
            for b, c in zip(self.axes, a.axes):
                b[index] = c
        else:
            for b in self.axes:
                b[index] = a

    def __show_init__(self, fignum, ax,  plt_fig_args, plt_args, plt_functions):
        """
        common initialisation for show

        """
        if not self.load():
            raise ValueError('Nothing to load or show')

        self.plt_fig_args.update(plt_fig_args)
        self.plt_args.update(plt_args)
        for funct in plt_functions:
            self.plt_functions = funct
        if ax is None:
            fig = plt.figure(fignum, **self.plt_fig_args)
            ax = fig.add_subplot(111)
        else:
            fig = ax.get_figure()

        return fig, ax


#-------------------------------------------------------------------------------
#properties

    @property
    @calc_once
    def name(self):
        # override default to strip extensions
        name = super(IM, self).name
        n,ext = os.path.splitext(name)
        if ext in self._extensions:
            return n
        else:
            return name

    @property
    def frames(self):
            return self.data.get('frames')

    @property
    def shape(self):
        return self.frames, self.buffer.ny, self.buffer.nx

    @property
    @calc_once
    def axes(self):
        """ All of the relevant axes with image or vector data
        """
        # can change this to suit the image_sub_type
        t = self.buffer.image_sub_type

        # 2D vectors
        if  t in [  ReadIM.core.BUFFER_FORMAT_VECTOR_2D              ,
                    ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED     ,
                    ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED_PEAK,
                    ]:
                axes = [getattr(self,'Vx'), getattr(self,'Vy')]

        # Images
        elif t <= 0:
            axes = [getattr(self,'I')]

        # 3D vectors
        elif t in [ReadIM.core.BUFFER_FORMAT_VECTOR_3D,
                    ReadIM.core.BUFFER_FORMAT_VECTOR_3D_EXTENDED_PEAK]:
                        axes = [self.Vx, self.Vy, self.Vz]
        else:
            raise TypeError('Buffer type unrecognised')

        return axes

##    @property
    def axesmasked(self):
        """ All of the relevant axes with image or vector data
        """
        # can change this to suit the image_sub_type
        t = self.buffer.image_sub_type

    #2D vectors
        if  t in [  ReadIM.core.BUFFER_FORMAT_VECTOR_2D              ,
                    ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED     ,
                    ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED_PEAK,
                    ]:
                axes = [getattr(self,'maVx')(), getattr(self,'maVy')()]
        #3D vectors
        if  t in [  ReadIM.core.BUFFER_FORMAT_VECTOR_3D              ,
                    ReadIM.core.BUFFER_FORMAT_VECTOR_3D_EXTENDED_PEAK,
                    ]:
                axes = [getattr(self,'maVx')(), getattr(self,'maVy')(), getattr(self,'maVz')()]
    #images
        elif t <= 0:
            axes = [getattr(self,'maI')()]
        else:
            raise TypeError('Buffer type unrecognised')
        return axes

    @property
    def applied_filters(self):
        return self.data['applied_filters']

    @property
    def active_masks(self):
        return self._active_masks

    @staticmethod
    def setactive_masks(value):
        active = IM._active_masks
        while active:
            active.pop()
        for msk in iterflatten(value):
            if not msk:
                continue
            if msk in IM.mask_coordinates:
                active.add(msk)
            else:
                raise IndexError('Missing mask named:"%s"\n from IM.mask coordinates \navailable mask names=\n%s' %(msk,list(IM.mask_coordinates.keys())))

    @property
    def mask(self):
        if not hasattr(self,'_maskarray'):
            self._maskarray = np.zeros(self.shape, dtype=bool)

        ma = self._maskarray
        # do atleast once or redo the mask if it is diff to the applied masks
        if (self.active_masks != self.__applied_masks__):
            # reset the mask
            ma[:] = False
            self.__applied_masks__.clear()
            all_masks = np.zeros(self.shape, dtype = bool)
            for mask_name in self.active_masks:
                self.__applied_masks__.add(mask_name)
                mask = self.make_mask(mask_name)
                all_masks = (all_masks | mask) # Reveal pixels based on mask area
            ma[:] = all_masks | self.original_mask # Cannot reveal anything outside original (invalid data)
        return ma

    @property
    def loadfiles(self):
        loadfiles = self.data.get('loadfiles', '')
        try:
            for f in loadfiles: assert(f)

        except AssertionError:
            old_root = self.data['loadfiles_root']
            loadfiles = [os.path.join(self.root, os.path.relpath(f,old_root))
                                for f in loadfiles]
            for f in loadfiles: assert(f)

        return loadfiles

    @loadfiles.setter
    def loadfiles(self,loadfiles):

        if len(loadfiles) != self.frames:
            raise IndexError('incorrect length of loadfiles')
        self.data['loadfiles'] = list(loadfiles)
        self.data['loadfiles_root'] = self.root


    @property
    def buffer(self):
        if not hasattr(self, '_buffer'):
            buff = self._data.get('buffer')
            if (not buff) and self.loadfile:
                assert self.load()
                buff = self._data.get('buffer')
                assert buff, 'Failed to load buffer from file.'\
                                    'Buffer unknown!%s' % repr(self)
            try:
                self._buffer = BufferTypeAlt(buff, immutable=True)
            except:
                raise TypeError('Unable to load buffer for: {0}'.format(self))
        return self._buffer

    def _set_buffer(self, buff, DestroyBuffer):
        buff = BufferTypeAlt(buff, DestroyBuffer, immutable=True)
        buff_d = {}
        ReadIM.extra.obj2dict(buff, buff_d)
        self._data['buffer'] = buff_d

    @property
    def frames(self):
        return self.buffer.nf

    @property
    def nx(self):
        return self.buffer.nx

    @property
    def ny(self):
        return self.buffer.ny

    @property
    @calc_once
    def window(self):
        window = []
        window.append(self.map_xy(0,0))
        window.append(self.map_xy(self.nx,self.ny))
        return window

    @property
    @calc_once
    def extent(self):
        w = self.window
        return [w[0][0],w[1][0],w[1][1], w[0][1]]

    @property
    @calc_once
    def dx(self):
        return abs((self.window[1][0] - self.window[0][0]) / (self.nx))

    @property
    @calc_once
    def dy(self):
        return abs((self.window[0][1] - self.window[1][1]) / (self.ny))

    @property
    @calc_once
    def Px(self):
        x,y = np.meshgrid(list(range(self.nx)), list(range(self.ny)))
        return self.map_xy(x,y)[0]

    @property
    @calc_once
    def Py(self):
        x,y = np.meshgrid(list(range(self.nx)), list(range(self.ny)))
        return self.map_xy(x,y)[1]

    @property
    def attributes(self):
        return self.data['attributes']

##    @property
    def mag(self):
        """
        Magnitude of self as an IM7.
        mag == sum(axes**2)**0.5
        For an IM7 this is equivalent to abs.

        Magnitude being the length of the vector which is the Root sum square
        of the components.

        Returns
        -------
        im: im7
        """
        im = self.new_im7('mag',append_name=True)
        I = im.I
        I[:] = 0
        for ax in self.axes:
            I += ax **2
        I[:] = I**0.5
        if not (im.I == I).data.all():
            raise RuntimeError('Problem setting data in magnitude')
        return im

##    @property
    def mamag(self):
        """
        Get magnitude of self with self mask applied

        Returns:
        -------
        im: im7
        """
        im = self.mag
        im.I.mask[:] = self.mask
        return im
#-------------------------------------------------------------------------------
#methods

    def set_plt_defaults(self):
        self.plt_functions = "plt.xlabel('x (mm)')"
        self.plt_functions = "plt.('z (mm)')"
        self.plt_functions = "plt.('scaled')" #scaled or equal

    def _getarray(self, key):

        if key not in self.data:
            self._data[key] = np.ma.array(np.zeros(self.shape),
                                mask=np.ones(self.shape), dtype=self._dtype)
            self._loaded = True
        return self.data[key]

    def _setarray(self, key, ar):

        # This is no longer valid
        if hasattr(self, '_maskarray'):
            del(self._maskarray)
        if ar.shape[-2:] != self.shape[-2:]:
            raise IndexError('Attempting to assign incorrect shape array %s to %s' %(ar.shape,self.shape))
        else:
            self._getarray(key)[:] = ar
            if np.ma.isMaskedArray(ar):
                self._getarray(key).mask[:] = ar.mask

    def _raw_buffer_as_array(self, buff):
        """
        Make a buffer in memory for ReadIM read write access returning an array
        interface.

        Parameters:
        ----------
        buff: BufferType | BufferTypeAlt

        Returns:
        -------
        vbuff: ndarray
        buff: BufferType

        Notes:
        -----
        The buffer must be destroyed after use with ReadIM.DestroyBuffer(buff).
        """

        if buff is None:
            buff = self._get_Buffer_()
        vbuff, buff = ReadIM.buffer_as_array(buff)
        return vbuff, buff


    def get_pixels(self, thin=1):
        """
        Returns a zipped list of all pixels.
        Useful for enumeration

        Parameters
        ----------
        thin: integer
            Will select every 'thin'th pixel in both directions.
                eg. thin = 2 will select every second pixel in both directions
                giving 1/(2*2) -> 1/4 pixels
        Returns
        -------
        pixels: list
            list of pixels
        """
        pixels = np.meshgrid(list(range(self.ny))[0:self.ny:thin],
                            list(range(self.nx))[0:self.nx:thin])
        return list(zip(pixels[0].flatten(),pixels[1].flatten()))

    def map_pix(self,pix):
        """
        Map the pixel coordinates to real world coordinates (the reverse of map XY)
        pix is (row,col)
        Parameters
        ----------
        pix: (y,x)


        Returns
        -------
        pix
        """
        y,x = pix
        return self.map_xy(x,y)

    def _get_Buffer_(self):
        """
        Get buffer from self.loadfile. Only valid
        for IM7 and VC7 filetypes.
        Returns
        -------
        buff: BufferType
        """
        return ReadIM.get_Buffer_andAttributeList(self.loadfile,atts=None)[0]

    def _get_Buffer_andAttributeList(self):
        """
        Get buffer and Attribute list from self.loadfile. Only valid
        for IM7 and VC7 filetypes.
        Returns
        -------
        buff: BufferType
        atts: Attribute List
        """
        try:
            return ReadIM.get_Buffer_andAttributeList(self.loadfile)
        except IOError:
            raise IOError("Problem loading file: {0}.Message: ".format(
                                        self.loadfile, sys.exc_info()[1]) )


    def map_i(self, i):
        """ Map stored intensities to real intensities
        """
        return map_d(i,get_num(self.data['attributes']['_SCALE_I'])[0],get_num(self.data['attributes']['_SCALE_I'])[1], dtype=self._dtype)

    def map_I(self, I):
        """ Map real intensities to stored  intensities
        buffer.scaleI.factor should be set
        """
        return map_D(I,self.buffer.scaleI.factor,self.buffer.scaleI.offset, dtype=self._dtype)

    def map_XY(self, X, Y , dtype=int):
        """ Map physical coordinates to pixel coordinates
        (X,Y) is the real world plane
        returns (x,y) the pixel position
        """
        # note that capitals and non caps are different
        Ax = self.buffer.scaleX.factor
        Bx = self.buffer.scaleX.offset
        Ay = self.buffer.scaleY.factor
        By = self.buffer.scaleY.offset
        x = np.array(map_D(X, Ax, Bx)/self.buffer.vectorGrid, dtype=dtype)
        y = np.array(map_D(Y, Ay, By)/self.buffer.vectorGrid, dtype=dtype)
        return x,y

    def map_xy(self, x, y):
        """ Map pixel coordinates to physical coordinates
        (x,y) is the pixel position, (X,Y) is the real world position
        """
        # note that capitals and non caps are different
        Ax = self.buffer.scaleX.factor
        Bx = self.buffer.scaleX.offset
        Ay = self.buffer.scaleY.factor
        By = self.buffer.scaleY.offset
        X = map_d(x * self.buffer.vectorGrid, Ax, Bx)
        Y = map_d(y * self.buffer.vectorGrid, Ay, By)
        return X,Y

    def update_attributes_with_scales(self):
        """ update self.attributes with scaling information
        """
            #transfer buffer info from v. Useful for writing files.

        # purge the frame scales # probably develop a better way to handle this.
        for p in [att for att in self.attributes if att.find('FrameScale')==0]:
            self.attributes.pop(p)

        for p in [att for att in self.attributes if att.find('_SCALE_')==0]:
            self.attributes.pop(p)


        for a in self.buffer:
            if a.find('scale') == 0:
                sc = eval('self.buffer.%s'%a)


                # parse the info from the buffer in suitable format

                # only work for frame0 at present

                _key = '_SCALE_%s' % a[-1].upper()
                _val = '{0:0.6f} {1:0.6f}\n{2}\n\n'.format(
                        sc.factor, sc.offset, sc.unit.strip('\n').strip('\x00'))
                self.attributes[_key] = _val

                val = '{0:0.6g}\n{1:0.6g}\n{2}\n'.format(
                        sc.factor, sc.offset, sc.unit.strip('\n').strip('\x00'))
                key = 'FrameScale%s0' % a[-1].upper()
                self.attributes[key] = val



    def copy(self, name='copy', force_new=True, root='',
                append_name=True, frames=None, family=None):
        """ return a copy of self
        """
        root= root or self.root
        name, root = self.get_new_name(name, force_new, append_name, root)

        family = family or self.family

        buff        = BufferTypeAlt(self.buffer)
        if frames:
            buff.nf = frames

        if not force_new:
            try:
                return self.__class__(name, root, family=famly)
            except:
                pass
        im = self.get_obj(self.__class__, name, buff=buff, root=root,
        family=family, dtype=self._dtype, force_new=force_new)
        if im.loaded:
            raise RuntimeError('Object already loaded - may cause unexpected behaviour')
        if not frames:
            if self.loaded == True:
                im._data.update(copy.deepcopy(self.data))

                im._loaded = True
                im._loadfile = '<buffer>repr={0}'.format(repr(self))
            else:
                im._loadfile = self.loadfile
        return im

    def _new_im(self, image_sub_type, frames=None, name='new_im',
                inherit_variables=False, force_new=True, root='',
                append_name=False, cls=None):
        """ returns either an IM7 or VC7 object depending on the image_sub_type
        **kwargs = name
        """
        im      = self
        name, root    = self.get_new_name(name, force_new, append_name, root)

        kwargs = dict()
        if frames is None:
            frames = self.frames

        if frames < 1:
            raise IndexError('One or more frames must be specified!')

        kwargs['frames'] = frames

        if root:
            kwargs['root'] = root

        if 'family' not in kwargs:
            kwargs['family'] = self.family

        if image_sub_type > ReadIM.core.BUFFER_FORMAT_IMAGE: # vector subtypes are greater than zero

            if isinstance(self, VC7):
                cls = self.__class__
                vectorGrid = im.buffer.vectorGrid
            else:
                cls = self._objects['VC7']
                vectorGrid = 24

        elif image_sub_type <= ReadIM.core.BUFFER_FORMAT_IMAGE:

            if isinstance(self, IM7):
                cls = self.__class__
            else:
                cls = self._objects['IM7']
            vectorGrid = 1

        buff        = ReadIM.newBuffer(im.window, im.nx, im.ny, vectorGrid , image_sub_type, frames)

        # this is required if spawning a new object
        # check if the object already exists
        fullname_new, rootnew, repnew = IM.get_fullname_root_and_rep(cls, name, **kwargs)

        if repnew in IM._instances:
            inst = IM._instances[repnew]
            name, root    = inst.get_new_name(name, force_new, append_name, root)

        new_im      = cls(name, buff, dtype=self._dtype, **kwargs)

        new_im.parents = self
        if inherit_variables:
            new_im.data['store'].update(self.data['store'])

        return new_im


    def new_vc7(self,name='new_im7', frames=None, inherit_variables=False, force_new=True, root='', append_name=False, image_sub_type=None):
        """ return a new instance with same buffer with the exception
            that the number of frames can be changed
            image_sub_type: if self is a vector will inherit self vector type otherwis default is 2D. See: ReadIM.core.BUFFER_FORMAT_###
        """
        if image_sub_type is not None:
            pass
        elif self.buffer.image_sub_type > 0:
            # vectors inherit vector sub type
            image_sub_type = self.buffer.image_sub_type
        else:
            image_sub_type = ReadIM.core.BUFFER_FORMAT_VECTOR_2D
        im = self._new_im(image_sub_type,frames, name, inherit_variables, force_new, root=root, append_name=append_name)
        return im

    def new_im7(self,name='new_im7', frames=None, inherit_variables=False, force_new=True, root='', append_name=False, image_sub_type=None):
        """ return a new instance with same buffer with the exception
            that the number of frames can be changed
        """
        if image_sub_type == None:
            image_sub_type = ReadIM.core.BUFFER_FORMAT_IMAGE
        im = self._new_im(image_sub_type,frames, name, inherit_variables, force_new, root=root, append_name=append_name)
        return im


    def make_mask(self,mask_name, output = 'array'):
        """ generates a mask of the coords based on the real world coordinates using polygon
    assumes data has already been loaded and the buffer is still available for mapping
    returns an array of the same nx, and ny as self which can be used to
    set the mask of the self
    output = 'array' or 'im' where 'im' is an produced by the module Image
        """
    # deal with coordintes passed as strings
        mask_names = list(self.mask_coordinates.keys())
        if mask_name in mask_names:
            coords = self.mask_coordinates[mask_name]
            return self.__make_mask__(coords, True, output)
        else:
            raise TypeError("mask_name not in list %s,\n%s" %(mask_name,mask_names))

    def __make_mask__(self,coords, real_world = True, output = 'array'):
        """ generates a mask of the coords based on the real world coordinates using polygon
    assumes data has already been loaded and the buffer is still available for mapping
    returns an array of the same nx, and ny as self which can be used to
    set the mask of the self
    output = 'array' or 'im' where 'im' is an produced by the module Image
        """
        # deal with coordintes passed as strings
        if type(coords[0]) == str:
            coords = [eval(l) for l in coords]
        v_grid = self.buffer.vectorGrid
        if real_world:
            pixel_positions = []
            for X,Y in coords:
                x,y = self.map_XY(X,Y)
                x *= v_grid
                y *= v_grid
                pixel_positions.append((x,y))
        else:
            pixel_positions = coords
        im = Image.new('L',(self.nx * v_grid,self.ny * v_grid))
        draw = ImageDraw.Draw(im)
        if not len(pixel_positions) < 2:
            draw.polygon(pixel_positions, fill = 1000)
        single_frame = np.invert(np.array(im, dtype = bool))
        st = 0 #v_grid/2 # the middle pixel (integer)
        msk = single_frame[st::v_grid,st::v_grid]
        if output == 'array':
            return msk
        elif output == 'im':
            return im
        else:
            raise TypeError('unknown output specified, use either: "array" or "im"')

    def __make_mask_circle__(self, centre, radius, real_world = True, output = 'array'):
        """ generates a mask of the coords based on the real world coordinates using polygon
    assumes data has already been loaded and the buffer is still available for mapping
    returns an array of the same nx, and ny as self which can be used to
    set the mask of the self
    output = 'array' or 'im' where 'im' is an produced by the module Image
    centre = (x,y) (image units if real_world)
    radius = r     (image units if real_world)
        """
        # deal with coordintes passed as strings

        coords = [[centre-radius, centre+radius], [centre+radius, centre-radius]]
        v_grid = self.buffer.vectorGrid
        if real_world:
            pixel_positions = []
            for X,Y in coords:
                x,y = self.map_XY(X,Y)
                x *= v_grid
                y *= v_grid
                pixel_positions.append((x,y))
        else:
            pixel_positions = coords
        im = Image.new('L',(self.nx * v_grid,self.ny * v_grid))
        draw = ImageDraw.Draw(im)
        if not len(pixel_positions) < 2:
            draw.ellipse(pixel_positions, fill = 1000)
        single_frame = np.invert(np.array(im, dtype = bool))
        st = 0 #v_grid/2 # the middle pixel (integer)
        msk = single_frame[st::v_grid,st::v_grid]
        if output == 'array':
            return msk
        elif output == 'im':
            return im
        else:
            raise TypeError('unknown output specified, use either: "array" or "im"')

    def load_pickled(self, quiet = False, force_reload = False, **kwargs):
        if self.loaded:
            return True
        if isinstance(self.loadfile,IM) or os.path.splitext(self.loadfile)[1] not in ['.vc7', '.VC7','.IM7', '.im7']:

            return super(IM, self).load(quiet=quiet, force_reload=force_reload, **kwargs)

    def draw_mask_edges(self,ax=None, *names, **kwargs):
        """ draws the outline of the names, or the active masks on the active figure
        """
        masks   = []
        ls      = []
        for name in names:
            if name in self.mask_coordinates:
                masks.append(name)
        if not masks:
            masks = self.active_masks

        for mask_name in masks:
            lines = self.mask_coordinates[mask_name]
            ls.extend(self.draw_lines(lines, ax, **kwargs))
        return ls


    def draw_lines(self, lines, ax=None, **kwargs):
        X = []
        Y = []
        if type(lines[0]) == str:
            lines = [eval(l) for l in lines]
        for x,y in lines:
            X.append(x); Y.append(y)
        if ax is None:
            ax = plt.gca()
        return ax.plot(X,Y,'k', **kwargs)

    def draw_window(self,label = 'window',ax = None, window = None, **kwargs):
        """ draws self.window on the active figure
        """
        if window is None:
            window = self.window
        p1 = window[0]
        p3 = window[1]
        p2 = (p3[0],p1[1])
        p4 = (p1[0],p3[1])
        line = [p1,p2,p3,p4,p1]
        X = [p[0] for p in line]
        Y = [p[1] for p in line]
        if ax is None:
            ax = plt.gca()
        return ax.plot(X,Y,label = label, **kwargs)

    def plot_pixel_position(self, row, column,ax = None, *args,**kwargs):
        X,Y = self.map_xy(row,column)
        if ax is None:
            ax = plt.gca()
        return ax.plot(X,Y,*args,**kwargs)

    def to_gif(self, dst=None, delay = 10,extn = 'png', frames = None, fignum = None, savefigargs={}):
        """ save as an animated gif, set all plot arguments first
        extn is the intermediate format used for creating the gif
        delay is deci seconds, i.e. delay = 10 == 0.1 seconds
        """
        if frames is None:
            frames = list(range(self.frames))
        to_gif_func([self.show],frames,dst,fignum,delay,extn,True, savefigargs)

class IM7(IM):
    """ Class to load .im7 and .image files with high level methods including
    show() and browse().
    I is array of dat.
    maI is a masked copy of I
    """
    _type = 'image'
    _extensions = ['.im7', '.IM7', '.image']

    @__storage_init__
    def __init__(self, name= '', buff=None, force_reload=False,
                 dtype=None, **kwargs):
        """
        """

        if self.loaded != 'initialising':
            return
        buff = super(IM7, self).__init__(name, buff, force_reload,
                                         dtype=dtype, **kwargs)
        self._slicer = IMSlicer(self)

    def set_plt_defaults(self):
        self.plt_functions = "plt.xlabel('x (mm)')"
        self.plt_functions = "plt.ylabel('z (mm)')"
        self.plt_functions = "plt.axis('scaled')" #scaled or equal
        self.plt_functions =  "plt.colorbar(mappable,ax=ax)"

#-------------------------------------------------------------------------------
#properties
#sl
    def _getsl(self):
        return self._slicer
    sl = property(_getsl)
#I
    def _getI(self):
        return self._getarray('I')
    def _setI(self,I):
        return self._setarray('I',I)
    I = property(_getI,_setI)
#maI
    def getmaI(self):
        maI = np.ma.copy(self.I)
        maI.mask[self.mask] = True
        return maI
    maI = property(getmaI)
#original_mask
    def getoriginal_mask(self):
        return self.I.mask
    original_mask = property(getoriginal_mask)

#-------------------------------------------------------------------------------
#methods

    def _getImage(self):
        """ Load the image from the the buffer/file (for .IM7 files)
        """

        extn = os.path.splitext(self.loadfile)[1]
        if not extn in ['.im7', '.IM7']:
            return

        try:
            buff, atts = self._get_Buffer_andAttributeList()
            self._data['attributes'].update(ReadIM.extra.att2dict(atts))
            vbuff, buff = self._raw_buffer_as_array(buff)
##            vbuff *= buff.scaleI.factor
##            vbuff += buff.scaleI.offset
            if buff.image_sub_type > 0:
                raise TypeError('buffer does not contain an image type = %s' % buff.image_sub_type)
            self._set_buffer(buff, False)
            vbuff = self.map_i(vbuff)
            mask, buff = ReadIM.extra.buffer_mask_as_array(buff)
            self._setarray('I', np.ma.array(vbuff, mask=(vbuff==0)))
            self._loaded = True
            return True

        finally:
            if ('vbuff' in dir()):
                del(vbuff) # Yes you need to do this to stop memory leaks
            if ('buff' in dir()):
                ReadIM.DestroyBuffer(buff)
                ReadIM.DestroyAttributeListSafe(atts)


    def _im7_writer(self, dst):
        """ write the current buffer as masked to dst
        """

        buff = BufferTypeAlt(self.buffer)
        #buff.isFloat = self._dtype == 'float'
        buff.isFloat = int((self.I % 1).any())  # A better test

        try:

    # DaVis objects
            buff, error_code= ReadIM.extra.createBuffer(buff)
            if error_code > 1:
                raise IOError('Error code %s=%s' %(ReadIM.ERROR_CODES[error_code], error_code))

            vbuff, buff = self._raw_buffer_as_array(buff)

            self.update_attributes_with_scales()
            atts = ReadIM.load_AttributeList(self.attributes)

            vbuff[:]        = 0
            setzero         = self.mask == False #swap

            vbuff[:]   =  self.map_I(self.I) * setzero


    # write from buffer
            err = ReadIM.WriteIM7(dst, True, buff, atts.next)

            if err:
                codes = codes = dict([(getattr(ReadIM.core,f),f) for f in dir(ReadIM.core) if f.find('IMREAD_ERR')==0])
                if err == 1:
                    print('Note that you cannot write into the top level directory for some strange reason')
                raise IOError('Error writing file: %s=%s' % (err, codes[err]))

            print('Created: %s' % dst)

    # clean up
        finally:
            if 'vbuff' in dir():
                del(vbuff) # Yes you need to do this to stop memory leaks
                ReadIM.DestroyBuffer(buff)
                if 'atts' in dir():
                    ReadIM.DestroyAttributeListSafe(atts)

    def writeIM7(self, dst):
        """ write the data to a .im7 file
            if frame is None dst assumes that dst is a directory
            else
        """
        if not re.search('.im7',dst, flags=re.IGNORECASE):
            dst = dst+'.im7'
        if not os.path.isabs(dst):
            dst = os.path.join(self.root, dst)
        common.make_dir_passive(os.path.dirname(dst))
        self._im7_writer(dst)
        return dst


    def show(self, frame=0, fignum=None, ax=None, cax=None,
            plt_fig_args={}, plt_args={}, plt_functions={}, reset_limits=False,
            set_plt_defaults=False):
        """ plots the frame number in fig or a new figure
            returns handles Q and f
            I = quiver() handle
            f = figure() handle
            If the data is imaginary then the magnitude is plotted.
        """

        if set_plt_defaults:
            self.set_plt_defaults()

        fig, ax = self.__show_init__(fignum, ax, plt_fig_args, plt_args,plt_functions)

        if hasattr(self.I, 'imag') and (self.I.imag > 0).any():
            maI = abs(self.maI)
        else:
            maI = self.maI

        vmin = self.plt_args.get('vmin',float(np.nanmin(maI.data)))
        vmax = self.plt_args.get('vmax',float(np.nanmax(maI.data)))
        self.plt_args = 'vmin', vmin
        self.plt_args = 'vmax', vmax


        A = ax.imshow( maI[frame], extent=self.extent, **self.plt_args)

        self._plt_functions_eval_all(fig,A,ax,None)
        return A, fig

    def load(self, quiet = False, force_reload = False, **kwargs):
        """ loads self from savepath first if available, otherwise loadfile
            does nothing if there is no sourcefile
        """

        if self.load_pickled(quiet=quiet, force_reload=force_reload, **kwargs):
            return True
        elif self._getImage():
            return True
        else:
            return False

    def browse(self,**kwarg):
        if not self.data.get('plt_defaults'):
            self.set_plt_defaults()

        b = IMBrowser(self,**kwarg)
        b.fig.canvas.draw()
        return b


class VC7(IM):
    """ Class to load .vc7 and .vector files. Methods include: show() and browse()
    array access via .Vx and .Vy
##    """
    _type = 'vector'
    _extensions = ['.vc7', '.VC7', '.vector']

    def __init__(self, name = '', buff=None, force_reload=False, **kwargs):
        """Vector object. shape is (frames, ny, nx) for Vx and Vy axes. Only
        2D at present.
        minimum to create a new buffer is VC7(buff=new_buffer())
        """
        if self.loaded != 'initialising':
            return
        buff = super(VC7, self).__init__(name, buff, force_reload, **kwargs)
        self._slicer = VecSlicer(self)

    def __reset__(self):

        super(VC7,self).__reset__()

            # plt_args
        for item in list({
        'scale':5,
        'units':'height',
        'width':0.0022,
        'headwidth':2.3,}.items()):
            self.plt_args = item

#-------------------------------------------------------------------------------
# plot defaults

    def set_plt_defaults(self):

        self.plt_functions = "plt.xlabel('x (mm)')"
        self.plt_functions = "plt.ylabel('z (mm)')"
        self.plt_functions = "plt.axis('scaled')" #scaled or equal
        #default functions
        self.plt_functions = "plt.quiverkey(mappable, 0.46, 0.92, 0.5, '0.5m/s'"\
                             ", labelpos='N',fontproperties=dict(weight='bold'))"
        self.plt_functions =  "plt.colorbar(mappable,cax,ax)"
        #default thinning
        self.plt_reduce_x = 2
        self.plt_reduce_y = 2

        for item in list({
        'scale':5,
        'units':'height',
        'width':0.0022,
        'headwidth':2.3,}.items()):
            self.plt_args = item


#-------------------------------------------------------------------------------
#properties
#Vx
    @property
    def Vx(self):
        return self._getarray('Vx')

    @Vx.setter
    def Vx(self, Vx):
        self._setarray('Vx',Vx)

    @property
    def Vy(self):
        return self._getarray('Vy')

    @Vy.setter
    def Vy(self, Vy):
        self._setarray('Vy',Vy)

    @property
    def Vz(self):
        return self._getarray('Vz')

    @Vz.setter
    def Vz(self, Vz):
        self._setarray('Vz',Vz)


    def staticomplex(self):
        c = np.ma.array(self.maVx(), dtype = complex)()
        c.imag[:] = self.maVy()
        return c

    def angle(self):
        c       = self.complex
        angle   =  np.angle(c)
        return np.ma.array(angle,mask = c.mask)

    def maVx(self):
        maVx = np.ma.copy(self.Vx)
        maVx.mask[self.mask] = True
        return maVx

    def maVy(self):
        maVy = np.ma.copy(self.Vy)
        maVy.mask[self.mask] = True
        return maVy

    @property
    def sl(self):
        return self._slicer

    def _getplt_reduce_x(self):
        if not hasattr(self, '_plt_reduce_x'):
            self._plt_reduce_x = 1
        return self._plt_reduce_x
    def __setplt_reduce_x(self, val):
        if val < 1: val = 1
        self._plt_reduce_x = int(val)
    plt_reduce_x = property(_getplt_reduce_x, __setplt_reduce_x)

    def _getplt_reduce_y(self):
        if not hasattr(self, '_plt_reduce_y'):
            self._plt_reduce_y = 1
        return self._plt_reduce_y
    def __setplt_reduce_y(self, val):
        if val < 1: val = 1
        self._plt_reduce_y = int(val)
    plt_reduce_y = property(_getplt_reduce_y, __setplt_reduce_y)

    def getoriginal_mask(self):
        return (self.Vx.mask | self.Vy.mask)
    original_mask = property(getoriginal_mask)


#-------------------------------------------------------------------------------
# methods

    def _get_vectors(self, buff = None):
        """ get all vectors from self.loadfile (overwrites existing information)
        """
        # faster but still not optimised completely - multiple array copies
        # no big deal
        extn = os.path.splitext(self.loadfile)[1]
        if not extn in ['.vc7', '.VC7']:
            return
        # change the mode based on the filetype (at present only extended peak works)
        vbuff = None

        try:

            self._loaded = 'loading_raw'
            buff, atts = self._get_Buffer_andAttributeList()
            self._data['attributes'].update(ReadIM.extra.att2dict(atts))
            vbuff, buff = self._raw_buffer_as_array(buff)
            self._set_buffer(buff, False)
            tp = buff.image_sub_type
            if tp == ReadIM.core.BUFFER_FORMAT_IMAGE:
                raise TypeError('This is an image file not a vector file')


            orientation = -1 # corresponds to DaVis axis coordinate system (left/right handed)

            components = ReadIM.GetVectorComponents(buff.image_sub_type)
            Vx = self.Vx
            Vy = self.Vy

            # New advice from Lavision suggests special handling
            if tp == ReadIM.core.BUFFER_FORMAT_VECTOR_2D:
                offset = 0
                for f in range(buff.nf):
                    fr          = offset + f * components
                    Vx[f]  = self.map_i(vbuff[fr  ])
                    Vy[f]  = self.map_i(vbuff[fr+1]) * orientation

            elif tp == ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED_PEAK:
                if False: # depricated
##                if int(self.attributes['_DaVisVersion'][0]) < 6:
                    offset = 7 # DaVis version 7
                    for f in range(buff.nf):
                        fr          = offset + f * components
                        Vx[f]  = self.map_i(vbuff[fr  ])
                        Vy[f]  = self.map_i(vbuff[fr+1]) * orientation
                else:

                    for f in range(buff.nf):
                            best = vbuff[0+components*f]
                            for i in range(1, 5):
                                if i < 4:
                                    Vx[f] = Vx.data + self.map_i(vbuff[2*i-1+components*f]) * np.array(best==i,dtype=float)
                                    Vy[f] = Vy.data + self.map_i(vbuff[2*i+components*f])   * np.array(best==i,dtype=float) * orientation
                                else:
                                    Vx[f] = Vx.data + self.map_i(vbuff[2*i-1+components*f]) * np.array(best>=i,dtype=float)
                                    Vy[f] = Vy.data + self.map_i(vbuff[2*i+components*f])   * np.array(best>=i,dtype=float) * orientation
                            Vx.mask[f, best==0] = True
                            Vy.mask[f, best==0] = True

                    if (best==5).any():
                        self.notes = 'Post processing detected'
                    if (best==5).any():
                        self.notes = 'Smoothing detected'
                        
            elif tp == ReadIM.core.BUFFER_FORMAT_VECTOR_3D_EXTENDED_PEAK:
                Vz = self.Vz

                for f in range(buff.nf):
                    best = vbuff[0+components*f]
                    for i in range(1, 5):
                        if i < 4:
                            Vx[f] = Vx.data + self.map_i(vbuff[2*i-2+components*f]) * np.array(best==i,dtype=float)
                            Vy[f] = Vy.data + self.map_i(vbuff[2*i-1+components*f])   * np.array(best==i,dtype=float) * orientation
                            Vz[f] = Vz.data + self.map_i(vbuff[2*i+components*f]) * np.array(best==i,dtype=float)
                        else:
                            Vx[f] = Vx.data + self.map_i(vbuff[2*i-2+components*f]) * np.array(best>=i,dtype=float)
                            Vy[f] = Vy.data + self.map_i(vbuff[2*i-1+components*f])   * np.array(best>=i,dtype=float) * orientation
                            Vz[f] = Vz.data + self.map_i(vbuff[2*i+components*f]) * np.array(best==i,dtype=float)
                    Vx.mask[f, best==0] = True
                    Vy.mask[f, best==0] = True
                    Vz.mask[f, best==0] = True

                    if (best==5).any():
                        self.notes = 'Post processing detected'
                    if (best==6).any():
                        self.notes = 'Smoothing detected'

            elif tp == ReadIM.core.BUFFER_FORMAT_VECTOR_3D:
                offset = 0
                for f in range(buff.nf):
                    Vz = self.Vz
                    fr = offset + f * components

                    Vx[f]  = self.map_i(vbuff[fr  ])
                    Vy[f]  = self.map_i(vbuff[fr+1]) * orientation
                    Vz[f]  = self.map_i(vbuff[fr+2])

            else:
                raise Exception('Format undefined')


        # mask (mode 2)
            mask = (self.Vx == 0) & (self.Vy == 0)

       # apply (mode 3) DaVis V8 +
            try:
                if buff.bMaskArray:
                    m1, buff = ReadIM.extra.buffer_mask_as_array(buff)
                    mask[m1==False] = True #inverted compared to DaVis logic...
            except AttributeError:
                warnings.warn('You are using an old version of ReadIM! Consider getting the latest verison.')
                raise # FIND OUT WHERE THIS IS GOING WRONG IN NEWER VERSION

            self.Vx.mask[:] = mask
            self.Vy.mask[:] = mask

            if tp >= ReadIM.core.BUFFER_FORMAT_VECTOR_3D:
                self.Vz.mask[:] = mask

            self._loaded = True
            self.__loaded_from_file__ = True

        finally:
            del(vbuff) # Yes you need to do this to stop memory leaks
            ReadIM.DestroyBuffer(buff)
            ReadIM.DestroyAttributeListSafe(atts)
        return True

    def writeVC7(self, dstdir, frames=None):
        """
        Write the frames to separate .vc7 files.

        Returns
        -------
        dstdir: str
            absolute path to destination directory.

        """
        if frames is None:
            frames = list(range(self.frames))

        if not os.path.isabs(dstdir):
            dstdir = os.path.join(self.root, dstdir)
        dstdir = common.make_dir_passive(dstdir)


        # write the set file
        stn = dstdir+'.set'
        if sys.version_info < (3,0):  # python 2
            stn = u"{0}".format(stn)
            with io.open(stn, u'w', encoding='utf-8')as st:
                st.write(u'// set file <{0}> created by Python'.format(stn))
        else:
            stn = "{0}".format(stn)  # python 3
            with io.open(stn, 'w', encoding='utf-8')as st:
                st.write('// set file <{0}> created by Python'.format(stn))


        import tempfile
        d=tempfile.gettempdir()

        extn = self.__class__.__name__
        for frame in frames:

            src = os.path.join(d,'tempoutfile.%s'%extn)
            suffix = ''
            try:
                dst = davis.add_sequential_file(None, dstdir, suffix)
            except TypeError:
                self._vc7_writer(src, frame)
                dst = davis.add_sequential_file(src, dstdir, suffix, mode='move')
                print('Created: {0}'.format(dst))
        return dstdir



    def _vc7_writer(self, dst, frame):

        buff     = BufferTypeAlt(self.buffer)
        buff.nf  = 1

        VECTORS_2D = [ReadIM.core.BUFFER_FORMAT_VECTOR_2D,
                      ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED,
                      ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED_PEAK]

        VECTORS_3D = [ReadIM.core.BUFFER_FORMAT_VECTOR_3D,
                      ReadIM.core.BUFFER_FORMAT_VECTOR_3D_EXTENDED_PEAK]

        if buff.image_sub_type in VECTORS_2D:
            buff.image_sub_type = ReadIM.core.BUFFER_FORMAT_VECTOR_2D

        if buff.image_sub_type in VECTORS_3D:
            buff.image_sub_type = ReadIM.core.BUFFER_FORMAT_VECTOR_3D

        try:
    # DaVis objects
            vbuff, buff = self._raw_buffer_as_array(buff)

            self.update_attributes_with_scales()
            atts  = ReadIM.load_AttributeList(self.attributes)

            vbuff[:]        = 0
            setzero         = self.mask[frame] == False #swap
            orientation = -1 # corresponds to DaVis axis coordinate system (left/right handed)

            vbuff[0] =  self.map_I(self.Vx[frame]) * setzero
            vbuff[1] = orientation * self.map_I(self.Vy[frame]) * setzero

            if buff.image_sub_type in VECTORS_3D:
                vbuff[2] = self.map_I(self.Vz[frame]) * setzero

    # write from buffer
            err = ReadIM.WriteIM7(dst, False, buff, atts.next)
            if err: raise IOError('Error writing file: %s' % err)
            return dst

    # clean up
        finally:
            del(vbuff) # Yes you need to do this to stop memory leaks
            ReadIM.DestroyBuffer(buff)
            ReadIM.DestroyAttributeListSafe(atts)
            del(atts)


#-------------------------------------------------------------------------------
# loading DaVis or pickled files
    def load(self, quiet=False, force_reload=False, **kwargs):
        """ loads self from savepath first if available, otherwise loadfile
            does nothing if there is no sourcefile
        """

        if self.load_pickled(quiet, force_reload):
            return True
        elif self._get_vectors():
            return True
        else:
            return False

    def show(self, frame = 0, fignum = None, ax = None,cax=None, plt_fig_args = {},
             plt_args = {}, plt_functions = [], thin = None, monochrome = False,
             vmax = None, set_plt_defaults=False):
        """ plots the frame number in fig or a new figure
            returns handles Q and f
            Q = quiver() handle
            f = figure() handle
            plt_fig_args and plt_args must be dictionaries which are passed as arguments
            thin = x and y are thinned out by the same interger amount
            plt_functions is a string of the pylab function to be evaluated. has direct access to:
                self
                frame
        """

        if set_plt_defaults:
            self.set_plt_defaults()

        fig, ax = self.__show_init__(fignum, ax, plt_fig_args, plt_args,plt_functions)

        if thin:
            self.plt_reduce_x = thin
            self.plt_reduce_y = thin

        rx = self.plt_reduce_x
        ry = self.plt_reduce_y

        c = (self.maVx()**2 + self.maVy()**2)**0.5

        if monochrome:
            Q = ax.quiver( self.Px[::ry,::rx],
                         self.Py[::ry,::rx],
                         self.maVx()[frame,::ry,::rx],
                         self.maVy()[frame,::ry,::rx],
                         **self.plt_args)
        else:
            Q = ax.quiver( self.Px[::ry,::rx],
                         self.Py[::ry,::rx],
                         self.maVx()[frame,::ry,::rx],
                         self.maVy()[frame,::ry,::rx],
                        c[frame,::ry,::rx]
                        ,**self.plt_args)

        if vmax == None:
            try:
                Vmax = np.ceil(np.nanmax(c.data[c.mask==False])*2)/2
            except:
                Vmax = np.ceil(np.nanmax(c.data)*2)/2
            vmax = max(0.5,Vmax)
        Q.norm.vmax = vmax
        Q.norm.vmin = 0
        self._plt_functions_eval_all(fig, Q, ax, cax)
        return Q, fig


    def browse(self, **kwargs):
        """
        **kwargs = VecBrowser(**kwargs)
        """
        if not self.data.get('plt_defaults'):
            self.set_plt_defaults()

        b = VecBrowser(self,**kwargs)
        b.fig.canvas.draw()
        return b

class Slicer():
    """ Class to slice through array data for VC7 and IM7 classes.
    """

    def __init__(self, im):


        self._im = weakref.ref(im)
        self._row = 0
        self._col = 0


    @property
    def im(self):
        """The object being sliced (VC7 or IM7)
        """
        return self._im()


    @property
    def row(self):
        """Current row for slicing"""
        return self._row

    @row.setter
    def row(self,row):
        row = int(row)
        self._row = max(0,min(row,self.im.ny-1))

    @property
    def pix(self):
        """Current pixel (column, row) being sliced"""
        return self.row,self.col
    @pix.setter
    def pix(self,pix):
        self.row,self.col = pix

    @property
    def col(self):
        return self._col
    @col.setter
    def col(self,col):
        col = int(col)
        self._col = max(0,min(col,self.im.nx-1))

    def _getP(self):
        return self.im.variables.phase_f
    P = property(_getP)

class IMSlicer(Slicer):
    """slice an IM7 object working of currently set mask
    MI is a slice through maI at current pixel
    P  is a slice through variables.phase_f at current pixel (if it exists)
    slice frame returns a single frame (not pixel)
    """
    def __init__self(vec,row=0,col=0):
        Slicer.__init__(self,vec, row,col)

    @property
    def MI(self):
        return self.im.I[:, self.row,self.col]
    @MI.setter
    def MI(self,val):
        if np.ma.isMA(val):
            self.im.I[:, self.row,self.col].data[:] = val.data
            self.im.I[:, self.row,self.col].mask[:] = val.mask
        else:
            self.im.I[:, self.row,self.col][:] = val
            self.im.I[:, self.row,self.col].mask[:] = np.zeros(len(val),dtype=bool)

class VecSlicer(Slicer):
    """slice a VC7 object working of currently set mask
    Mx is a slice through maVx at current pixel
    Mx is a slice through maVy at current pixel
    P  is a slice through variables.phase_f at current pixel (if it exists)
    slice frame returns a single frame (not pixel)
    """

    def __init__self(vec,row=0,col=0):
        Slicer.__init__(self,vec, row,col)

    @property
    def Mx(self):
        return self.im.Vx[:, self.row,self.col]
    @Mx.setter
    def Mx(self,val):
        if np.ma.isMA(val):
            self.im.Vx[:, self.row,self.col].data[:] = val.data
            self.im.Vx[:, self.row,self.col].mask[:] = val.mask
        else:
            self.im.Vx[:, self.row,self.col][:]      = val
            self.im.Vx[:, self.row,self.col].mask[:] = np.zeros(len(val),dtype=bool)
            pass


    @property
    def My(self):
        return self.im.Vy[:, self.row,self.col]
    @My.setter
    def My(self,val):
        if np.ma.isMA(val):
            self.im.Vy[:, self.row,self.col].data[:] = val.data
            self.im.Vy[:, self.row,self.col].mask[:] = val.mask
        else:
            self.im.Vy[:, self.row,self.col][:]      = val
            self.im.Vy[:, self.row,self.col].mask[:] = np.zeros(len(val),dtype=bool)

            pass


    def slice_frame(self, frame):
        frame = int(frame)
        if frame < 0:frame = 0
        if frame >= self.im.frames: frame = self.im.frames -1
        rx = self.im.plt_reduce_x
        ry = self.im.plt_reduce_y
        return self.im.maVx()[frame,::ry,::rx],self.im.maVy()[frame,::ry,::rx],self.c[frame,::ry,::rx]

    @property
    def c(self):
        if not hasattr(self, '_c'):
            self._c = (self.im.maVx() **2 + self.im.maVy()**2)**0.5
        return self._c


class VC7u(VC7):
    """ Carry an sse with the main VC7 object.
    sse may be either a VC7 object or a single value
    """

    def __init__(self, name = '', buff=None, force_reload=False,
                    sse_type='float', **kwargs):
        """ sse_type = 'float' or in Storage._objects
        """

        buff = super(VC7u, self).__init__(name, buff, force_reload,
                                             **kwargs)

        if not hasattr(self, '_sse'):
            # control what objects can be used for sse
            sse_types = list(self._objects.keys()) + ['float']
            assert sse_type in sse_types, "sse_type should be in {0}".format(sse_types)

            self._sse_type = sse_type

            if sse_type in self._objects:
                if self.loadfile:
                    buff = None
                else:
                    buff  = self.buffer

                self._sse = self.get_obj(sse_type, self._sse_name, root=self.root,
                                            parents=self, buff=buff, family=self.family)
                if self.loadfile:
                    assert self.sse.loadfile, 'child should also have a loadfile.'

    @property
    def _sse_name(self):
        return '_'.join([self.name, 'u'])

    @property
    def sse(self):
        if hasattr(self, '_sse'):
            return self._sse
        else:
            if 'sse' in self.variables:
                self._sse = self.variables.sse
                return self._sse

            try:
                return self.get_obj('VC7', self._sse_name, root=self.root, parents=self, family=self.family)
            except IOError:
                raise
                return self.get_obj('VC7', self._sse_name, buff=self.buffer, root=self.root, parents=self, family=self.family)
                raise
                raise IOError('Unable to locate sse file for: {0}'.format(self))


    @ sse.setter
    def sse(self, value):
        return self.set_sse(value)

    def set_sse(self, value):
        if isinstance(value, VC7):

            # check for compatibility
            assert self.window == value.window
            assert self.shape == value.shape

            if value.name == self._sse_name and value.root == self.root:
                u = value
            else:
                try:
                    u=value.copy(self._sse_name, root=self.root, force_new=False, append_name=False)
                except RuntimeError:
                    raise RuntimeError('Need to handle this instance ToDo1')

            u.parents = self

            # a minor bug workaround ToDo: find out how an object may be passed which
            # is not in the instances list
            if u._instance_name not in self._instances:
                self._instances[u._instance_name] = u

            if os.path.isfile(str(self)) and not os.path.isfile(str(u)):
                u.save()

        else:
            if isinstance(value, float):
                u = np.array(value)
            else:
                u = value
            self.variables.store(sse=u)

        self._sse = u

class Browser():
    """
    Class to enable browsing capability for bowsing IM7 and VC7 classes using
    the show method
    """
    def __init__(self,im, **kwargs):
        """
        kwargs correspond to vec.show(), plus 'axframe' for frame control
        rect  = optional space to put the widgits
        """
        self.im = im

        if 'axframe' in kwargs:
            self.axframe  = kwargs.pop('axframe')
            if 'axphase_f' in kwargs:
                self.axphase_f = kwargs.pop('axphase_f')
            self.mappable,self.fig = im.show( **kwargs) #plt_args ={'picker':5},
        else:
            self.mappable,self.fig = im.show(**kwargs)
            self.axphase_f  = self.fig.add_axes([0.1, 0.05,0.8,0.02])
            self.axframe    = self.fig.add_axes([0.1, 0.03,0.8,0.02])

        # plot phase_f
        if hasattr(self.im.variables,'phase_f'):

##            self.axphase_f.plot(list(range(self.im.frames)),self.im.variaphase_fbles.,'k')
            self.phase_f_line = self.axphase_f.plot([0,0],[0,1])[0] # for update
##            self.phase_f_text = self.axphase_f.text(0.5, 0.5, 'phase_f = %0.4f'%self.im.variables.phase_f[0],
##                            transform=self.axphase_f.transAxes, va='center')

        # retain these so we don't have to make them all the time
        self.Px = im.Px
        self.Py = im.Py

        self.__showframe__ = None
        self.ax = self.mappable.axes
        self.__frame__ = Slider(self.axframe , 'Frame', 0, im.frames-1, valinit=0,valfmt='%3d',dragging = False)
        self.__frame__.on_changed(self.update)
        self.text = self.ax.text(0.05, 0.95, 'selected: none',
                            transform=self.ax.transAxes, va='top')
        mgrid = np.meshgrid(np.linspace(im.window[0][0],im.window[1][0]),np.linspace(im.window[0][1],im.window[1][1]))
        self.ax.plot(mgrid[0],mgrid[1],picker=5,visible=False)
        self.fig.canvas.mpl_connect('key_press_event', self.press)
        self.fig.canvas.mpl_connect('pick_event', self.onpick)
        self.selected,  = self.ax.plot(im.window[0][0], im.window[0][1], 'ro', ms=12, alpha=0.4,
                                  color='yellow', visible=False)
        self.callbacks = {}

    def _getpix(self):
        return self.im.sl.pix
    def _setpix(self, pix):
        self.im.sl.pix = pix
        self.update_pick()
    pix = property(_getpix, _setpix)

    def register_callback(self, name, handle):
        if name not in self.callbacks:
            self.callbacks[name] =[]
        self.callbacks[name].append(handle)

    def onpick(self, event):

       N = len(event.ind)
       if not N: return True

       # the click locations
       x = event.mouseevent.xdata
       y = event.mouseevent.ydata
       self.im.sl.col,self.im.sl.row = self.im.map_XY(x,y)
       self.update_pick(event)

    def update_pick(self, event = None):
       self.selected.set_visible(True)
       sl = self.im.sl
       self.selected.set_data(self.Px[sl.pix], self.Py[sl.pix])
       self.text.set_text("""row = %2i col = %2i""" %(sl.row,sl.col))
       if 'onpick' in self.callbacks:
            for obj in self.callbacks['onpick']:
                obj(event)
       self.fig.canvas.draw()


    def press(self,event):
        if event.key in ['alt+left', 'left']:
            frame = int(self.__frame__.val) - 1
            if frame < 0:
                frame = self.im.frames -1
            self.__frame__.set_val(frame)
            self.update()
        if event.key in ['alt+right', 'right']:
            frame = int(self.__frame__.val) + 1
            if frame >= self.im.frames:
                frame = 0
            self.__frame__.set_val(frame)
            self.update()

    def update(self, val = None):
        print('overwrite this function')

    def show_frame(self,frame,*args):
        frame = int(frame)
        if frame < 0:
            frame = self.im.frames -1
        if frame >= self.im.frames:
            frame = 0
        self.__frame__.set_val(frame)
        self.update()

    def to_gif(self, dst=None, delay = 10,extn = 'png', frames = None, savefigargs={}):
        """ save as an animated gif, set all plot arguments first
        extn is the intermediate format used for creating the gif
        delay is deci seconds, i.e. delay = 10 == 0.1 seconds
        """
        if frames is None:
            frames = list(range(self.im.frames))

        to_gif_func([self.show_frame],frames,dst,self.fig.number,delay,extn,False, savefigargs)

    def reset_im(self,im):
        self.__init__( im,   axphase_f   = self.axphase_f,
                             mappable    = self.mappable,
                             axframe     = self.axframe)



class VecBrowser(Browser):
    """Use this class to browse a VC7 object frame by frame. Provision is built in
    to pick a single pixel and to navigate throught the frames by a keypress.
    right arrow = next frame
    left arrow  = previous frame

    use the method: register_callback(name, handle) to associate a method (handle)
    with the picker.

    """

    def __init__(self,vec, **kwargs):

        Browser.__init__(self,vec, **kwargs)

    def update(self, val = None):
        frame = int(self.__frame__.val)
        if frame != self.__showframe__:

        # update phase_f
            if hasattr(self.im.variables,'phase_f'):
                phase_f = self.im.variables.phase_f[self.__frame__.val]
##                self.phase_f_text.set_text('phase_f = %0.4f'%phase_f)
##                self.phase_f_line.set_xdata([frame,frame])
        #
            self.mappable.set_UVC(*self.im.sl.slice_frame(frame))
            self.__showframe__ = frame
            if 'update' in  self.callbacks:
                for obj in self.callbacks['update']:
                    obj(self)
            self.fig.canvas.draw()

class IMBrowser(Browser):
    """Use this class to browse a IM object frame by frame. Provision is built in
    to pick a single pixel and to navigate throught the frames by a keypress.
    right arrow = next frame
    left arrow  = previous frame

    use the method: register_callback(name, handle) to associate a method (handle)
    with the picker.
    """
    def __init__(self,im,row=0,col=0,**kwargs):
        Browser.__init__(self, im,**kwargs)

    def update(self, val = None):
        frame = int(round(self.__frame__.val))
        if frame != self.__showframe__:

        # update phase_f
            if hasattr(self.im.variables,'phase_f'):
                phase_f = self.im.variables.phase_f[int(self.__frame__.val)]
                self.phase_f_text.set_text('phase_f = %0.4f'%phase_f)
                self.phase_f_line.set_xdata([frame,frame])

            if hasattr(self.im.I, 'imag') and (self.im.I.imag > 0).any():
                maI = abs(self.im.maI)
            else:
                maI = self.im.maI
            self.mappable.set_data(maI[frame])
            self.__showframe__ = int(frame)
            if 'update' in  self.callbacks:
                for obj in self.callbacks['update']:
                    obj(val)
            self.fig.canvas.draw()


# additional functions
#-------------------------------------------------------------------------------

def get_im_type(filename):
    """ a generic loader for IM7, VC7
    returns filetype, filename
    """
    fname, extn = os.path.splitext(filename)
    if extn in ['.pkl', '.h5']:
        fname, extn = os.path.splitext(fname)

    if extn in ['.im7','.IM7', '.image']: return 'image', filename
    elif extn in ['.vc7', '.VC7', '.vector']: return 'vector', filename
    else:
        raise TypeError('Filetype not recognised %s not in known extensions list' %(extn))

def loader(filename, force_reload = False, load = False, family=''):
    """ a generic loader for IM7, VC7, vector, image
    """

    if filename in Storage._instances:
        if force_reload:
            raise RuntimeError('Code requires development ToDo!')
        im = common.getobjFromRep(filename)
        return im


    if isinstance(filename, IM):
        return filename
    if not os.path.isfile(filename):
        try:
            obj = eval(filename, Storage._objects)
            if isinstance(obj, Storage):
                return obj
        except:
            pass

        raise IOError('file does not exist: %s' %filename)
    filetype, filename = get_im_type(filename)
    if filetype in ['image']:
        im = Storage._objects['IM7'](filename, buff = None, force_reload = force_reload, family=family)

    elif filetype in ['vector']:
        im =  Storage._objects['VC7'](filename, force_reload=force_reload, family=family)

    else:
        raise TypeError('Filetype not recognised %s not in known extensions list' %(filetype))
    return im


Storage._objects['VC7']     = VC7
Storage._objects['IM7']     = IM7
Storage._objects['VC7u']    = VC7u
Storage._objects['__IM__']  = IM # base class for images and vectors

def demo_VC7(family='default', **kwargs):
    '''
    demonstrates the creation of a new VC7 object.
    '''
    import ReadIM
    import numpy as np
    image_sub_type  = ReadIM.core.BUFFER_FORMAT_VECTOR_2D #2 for a vector 0 for an image
    buff            = ReadIM.newBuffer(window=((0,100),(100,0)),nx=20,ny=15,frames=2, image_sub_type=image_sub_type, alternate_buff=True)
    v   = VC7('demo',buff=buff, family=family, **kwargs)

    # fill with something
    v.Vx[:] = 0.5
    for f in range(v.frames):
        for x in range(v.ny):
            v.Vy[f,x,:] = np.linspace(0.3,0.8,v.nx) + f

    return v


def demo_IM7(family='default', **kwargs):
    '''
    demonstrates the creation of a new IM7 object.
    '''
    import ReadIM
    import numpy as np
    image_sub_type  = ReadIM.core.BUFFER_FORMAT_IMAGE
    buff            = ReadIM.newBuffer(window=((0,100),(100,0)),nx=20,ny=15,frames=2, image_sub_type=image_sub_type, alternate_buff=True)
    im   = IM7('demo',buff=buff, family=family, **kwargs)

    # fill with something
    im[:] = 0.5
    for f in range(im.frames):
        for x in range(im.ny):
            im[f,x,:] = np.linspace(0.3,0.8,im.nx) + f

    return im

if __name__=='__main__':

    pass


