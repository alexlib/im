Overview
========
IM provides higher level access to DaVis native files im7 and vc7 and possibly also older imx formats (untested).
Files are loaded via classes IM7 and VC7 and stored in
Python friendly formats which enables storge and loading via pickling.

Note: Experimental support for 3D vector fields. 

requires:
ReadIM
PIL
numpy
Matplotlib
scipy

optional:
imagemagick (executable binary not python library) used for generation of animated gifs.

Installation
============
Run setup in the usual way.
Should install dependencies automatically (untested)

>>python setup.py install


Optional File association

To associate  IM type files (.vector and .image files) and DaVis type files (.vc7, .im7)
in windows by doing the following:

1. run associate.py which will create a file called associate.bat
2. run associate.bat with administrative rights (shift + right click --> run as administrator)
3. check it works. Go into ./sample and double click an image or vector.
4. You can change frames by using left and right arrow keys on your keyboard.


Usage
=====

    >>> import IM
    >>> v=IM.demo_VC7()

Visualise the velocity field
    >>> v.browse()
    <IM.core.VecBrowser object at 0x000000000B454710>
    >>> v.show()
    (<matplotlib.quiver.Quiver object at 0x000000000DD7C470>,
     <matplotlib.figure.Figure object at 0x000000000B2F6748>)

    >>> v.Vx.shape
    (2L, 15L, 20L)
    >>> v.Vy.shape
    (2L, 15L, 20L)
    >>> v.frames
    2

Some numpy functionality is enabled (if rational)

slicing frames

    >>> v[0:1]
    <VC7> C:\Program Files\PyScripter\demo_sl.vector

maximum (across all frames)
    >>> v.max()
    <VC7> C:\Program Files\PyScripter\demo_amax=numpy.vector
    >>> v.max().frames
    1

maximum (for x and y velocity fields)
    >>> v.max(None)
    array([ 0.5,  1.8])

IM Works well in combination with an IDE. I recommend PyScripter on windows.