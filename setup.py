#!/usr/bin/env python

"""
Setup IM
"""
version = '0.91'

from setuptools import setup, Extension
import io
import sys
# enable double click
if len(sys.argv) < 2:
    ##    script_args = ['bdist_wininst']
    script_args = ['build', 'install']
else:
    script_args = sys.argv[1:]

long_description = ''.join(io.open('README').readlines())

setup(name='IM',
      version=version,
      author="Alan Fleming",
      author_email='alanf@amc.edu.au',
      url='https://bitbucket.org/fleming79/im',
      description="IM - Read, write and perform operations on DaVis files: im7 and vc7",
      long_description=long_description,
      license='Creative Commons Attribution-NonCommercial 3.0 Unported License',
      install_requires=[
           'ReadIM>=0.6',
           'numpy>=1.6',
           'Pillow',
           'matplotlib>=1.1',
          'scipy>=0.1',
          'configobj>=5.0.6'
      ],
      dependency_links=['https://bitbucket.org/fleming79/readim',
                        'http://www.lfd.uci.edu/~gohlke/pythonlibs'],
      packages=['IM'],
      script_args=script_args
      )
