from __future__ import division, print_function, absolute_import
__metaclass__ = type

from matplotlib import cbook
import glob
from .common import copy_file_passive, make_dir_passive, iterflatten
import os
import shutil

#-------------------------------------------------------------------------
# functions


def add_sequential_file(src, dst_dir, suffix='', extn='.VC7',
                        digits=5, simulate=False, return_ptn=False, mode='copy'):
    """ Copies the source file to the dst_dir and names the new file as a
        the next file in a sequence. mode is 'copy' or 'move'
    """

    # try to repeat the pattern
    def mkname(im_key, suffix, extn): return (
        '%s%s%s' % (im_key, suffix, extn))

    if suffix:
        # ensure there is a '_' at the start of the suffix
        if suffix[0] != '_':
            suffix = '_' + suffix

    ptn = os.path.join(dst_dir, mkname('*', suffix, extn))

# allow the pattern to be returned
    if return_ptn:
        return ptn

    file_with_suffix = glob.glob(ptn)
    if file_with_suffix:
        return file_with_suffix[0]
    current_vectors = sorted(cbook.listFiles(
        dst_dir, '*' + extn, recurse=False))

    try:
        last_vector = os.path.split(current_vectors[-1])[1]
    except IndexError:
        last_vector = 'B' + digits * '0' + extn

    n = int(last_vector[1:digits + 1]) + 1

    im_key = 'B%%0.%sd' % (digits)
    im_key = im_key % n

    fname = mkname(im_key, suffix, extn)

    try:
        if not simulate:
            make_dir_passive(dst_dir)
            dst = os.path.join(dst_dir, fname)
            if mode == 'copy':
                shutil.copy(src, dst)

            if mode == 'move':
                os.rename(src, dst)

        return os.path.join(dst_dir, fname)
    except IOError:
        raise IOError
    # should look like 'B0000#.VC7
    return fname


def davis_path(base, set_template, simulate=False, *subfolders):
    """ Creates folders and copies files to suit the subfolders as required
        subfolders is a list of consecutively deeper directories
        base should be the base of the experiment
        set_file should be the path to a template set_file file
    """
    sub = base
    for folder in iterflatten(subfolders):
        sub = os.path.join(sub, str(folder))
        if not simulate:
            copy_file_passive(set_template, sub + '.set')
            make_dir_passive(sub)
    return sub
