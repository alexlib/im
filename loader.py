if __name__ == '__main__':
    import sys
    import os

    from IM.core import loader
    import pylab
    import Tkinter as Tk

    br = loader(sys.argv[1]).browse(plt_fig_args={'figsize':(12,8)})
    def destroy(e): sys.exit()
    pylab.show()
    Tk.mainloop()




#the following loads ipython beside. However the process is not closed when the
# window is closed and the matplotlib widgits aren't working.
#ToDo3 Fix these issues
if False:

    if __name__ == '__main__':
        pass
    if False:
        import sys
        from .core import loader
        import tkinter as Tk

        loader(sys.argv[1]).browse()
        def destroy(e): sys.exit()
        Tk.mainloop()

    if True: # the following is experimental to have a ipython console popup
        import sys
        from .core import loader
        import tkinter as Tk
        from matplotlib.widgets import Button
        import matplotlib.pyplot as plt

        #ToDo1 put in an interactive button to pop up in the namespace of the figure
        # see - http://wiki.ipython.org/Old_Embedding/Tkinter
        from ipython_view import IPythonView

        from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
        from matplotlib.figure import Figure
        plt.matplotlib.interactive(False)

        root = Tk.Tk()
        root.wm_title("Embedding in TK")

        im = loader(sys.argv[1])
        im.set_plt_defaults()
        br = im.browse()

        def openConsole():
            #Open an iPython console and pass the objects to the namespace
            #ToDo either nest this below or beside the object in the matplotlib
            #window or make it a popup button.
            s=IPythonView(root)
            s.updateNamespace(dict(im=im, br=br))
            s.pack(side=Tk.TOP)
    ##
    ##    axconsle = plt.axes([0.02, 0.95, 0.1, 0.05])
    ##    bnext = Button(axconsle, 'Console')
    ##    bnext.on_clicked(openConsole)



        f = br.ax.figure

        # a tk.DrawingArea
        canvas = FigureCanvasTkAgg(f, master=root)
        canvas.show()
        canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

        toolbar = NavigationToolbar2TkAgg( canvas, root )
        toolbar.update()
        canvas._tkcanvas.pack(side=Tk.LEFT, fill=Tk.BOTH, expand=1)

    ##    s=IPythonView(root)
    ##    s.updateNamespace(dict(im=im, br=br))
    ##    s.pack(side=Tk.RIGHT)

        def _quit():
            root.quit()     # stops mainloop
            root.destroy()  # this is necessary on Windows to prevent
                            # Fatal Python Error: PyEval_RestoreThread: NULL tstate

        button = Tk.Button(master=root, text='Quit', command=_quit)
        button.pack(side=Tk.BOTTOM)
    ##    button1 = Tk.Button(master=root, text='Console', command=openConsole)
    ##    button1.pack(side=Tk.BOTTOM)
        openConsole()
        Tk.mainloop()
        # If you put root.destroy() here, it will cause an error if
        # the window is closed with the window manager.
