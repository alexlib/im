
import os
import sys

if __name__ in ['__main__', 'test__np_methods']:
    pth = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
    sys.path.insert(0, pth)
from test_common import *
from test_IM import *
from test__np_methods import *
from test_filter import *
