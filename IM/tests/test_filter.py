
import os
import sys

if __name__ in ['__main__', 'test_filter']:
    pth = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
    sys.path.insert(0, pth)


##import unittest
##from unittest import TestCase

import matplotlib as mpl
##mpl.rcParams['backend'] = "Qt4Agg"
##mpl.rcParams['backend.qt4'] = "PySide"
from pylab import plt

import IM
from IM import *
import imp
imp.reload(IM)
import IM.filter
import scipy.ndimage as nd_image
import glob
#-------------------------------------------------------------------------

from numpy.testing import (TestCase, assert_almost_equal, assert_equal,
                           assert_, assert_raises, run_module_suite,
                           assert_allclose)

import shutil
from tempfile import mkdtemp
import gc
import numpy as np

#-------------------------------------------------------------------------
import ReadIM
files = ReadIM.extra.get_sample_image_filenames() + \
    ReadIM.extra.get_sample_vector_filenames()

src_dir = ReadIM.extra.get_sample_folder()

vectype_2D = [ReadIM.core.BUFFER_FORMAT_VECTOR_2D,
              ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED,
              ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED_PEAK,
              ]


def get_bfs():
    """
    Iterate through buffer formats available
    """
    for bf in ReadIM.BUFFER_FORMATS:
        if bf > 0 and bf not in vectype_2D:
            "buffer format not yet supported {0}".format(
                ReadIM.BUFFER_FORMATS[bf])
            continue
        yield bf


class TestFilters(TestCase):

    def setUp(self):

        self.pth = os.getcwd()
        self.tempdir = mkdtemp()

        IM.common.Storage.__storage__['root'] = self.tempdir
        os.chdir(self.tempdir)

    def tearDown(self):
        os.chdir(self.pth)
        shutil.rmtree(self.tempdir)
        gc.collect()
# plt.close('all')

    def testmake_common_buffer(self):

        buff1 = ReadIM.newBuffer([[0, 3], [5, 0]], frames=2)
        buff2 = ReadIM.newBuffer([[0, 1], [1, 0]], frames=2)
        v1 = IM.VC7('v1', buff=buff1)
        v2 = IM.VC7('v2', buff=buff2)
        buff3 = IM.filter.make_common_buffer([v1, v2])
        v3 = IM.VC7('v3', buff=buff3)
        assert_almost_equal(v3.dx, v1.dx)
        assert_almost_equal(v3.dy, v1.dy)

        assert_almost_equal(np.array(v3.window), np.array([[0, 3], [5, 0]]))

        buff4 = IM.filter.make_common_buffer([v1, v2], 0.5, 0.5)
        v4 = IM.VC7('v4', buff=buff4)
        assert_almost_equal(v3.window, [[0, 3], [5, 0]])
        assert_almost_equal(v4.dx, 0.5)
        assert_almost_equal(v4.dy, 0.5)

    def testresize(self):

        for bf in get_bfs():

            gc.collect()
            buff1 = ReadIM.newBuffer(
                [[0, 3], [5, 0]], frames=2, image_sub_type=bf)
            buff2 = ReadIM.newBuffer(
                [[0, 1], [1, 0]], frames=2, image_sub_type=bf)

            if bf > 0:
                cls = IM.VC7
            else:
                cls = IM.IM7

            v1 = cls('v1_{0}'.format(bf), buff=buff1)
            v1.variables.store(a='hello')

            v1[:] = 2.3
            v2 = IM.filter.resize(v1, buff2)
            assert_('a' in v2.variables, 'attribute not copied')

            v3 = cls('v3_{0}'.format(bf), buff=buff2)
            v3[:] = 2.3
            assert_((abs(v2 - v3) < 0.01).all())

            ims = [v1, v2]
            del(v1)
            del(v2)

            reps = [repr(v) for v in ims]
            for v in ims:
                del(v)
            del(ims)

            gc.collect()
            for r in reps:
                assert_(r not in IM.IM7._instances,
                        "failed to delete object: {0}".format(r))

    def testinterpFrames(self):
        for bf in get_bfs():

            gc.collect()
            buff1 = ReadIM.newBuffer(
                [[0, 3], [5, 0]], frames=12, image_sub_type=bf)

            if bf > 0:
                cls = IM.VC7
            else:
                cls = IM.IM7

            v1 = cls('v1_{0}'.format(bf), buff=buff1)
            for f in range(v1.frames):
                v1[f] = 1.2**f

            frames = np.r_[0.5:v1.frames]

            v2 = IM.filter.interpFrames(v1, frames, order=1)
            assert_((v2[:v2.frames - 1].mask == False).all(),
                    "There shouldn't be any masked data until the last frame")
            v3 = v2.__class__('v3', buff=v2.buffer)
            for i, f in enumerate(frames):
                v3[i] = 1.2**f

            assert_(
                (abs(v3[0:v3.frames - 1] - v2[0:v2.frames - 1]) < 0.1).all())

    def testresampleFrames(self):

        for bf in get_bfs():

            gc.collect()
            buff1 = ReadIM.newBuffer(
                [[0, 3], [5, 0]], frames=12, image_sub_type=bf)

            if bf > 0:
                cls = IM.VC7
            else:
                cls = IM.IM7

            v1 = cls('v1_{0}'.format(bf), buff=buff1)
            for f in range(v1.frames):
                v1[f] = 1.2**f

            frames = np.r_[0.5:v1.frames]

            M = 2
            v2 = IM.filter.resampleFrames(v1, M, order=3)
            assert_((v2[:v2.frames - 1].mask == False).all(),
                    "There shouldn't be any masked data until the last frame")
            v3 = v2.__class__('v3', buff=v2.buffer)

            for i, f in enumerate(np.linspace(0, v1.frames - 1, v3.frames)):
                v3[i] = 1.2**f

            assert_((abs(v3 - v2) < 0.1).all(),
                    'This shows that interpolation is inaccurate')


# assert_((abs(v3[0:v3.frames-1]-v2[0:v2.frames-1])<0.1).all())

    def testset_zero(self):

        for bf in get_bfs():

            gc.collect()
            buff1 = ReadIM.newBuffer([[0, 10], [10, 0]], 10, 10,
                                     frames=2, image_sub_type=bf,
                                     vectorGrid=1)

            if bf > 0:
                cls = IM.VC7
            else:
                cls = IM.IM7

            v1 = cls('v1_{0}'.format(bf), buff=buff1)
            v1[:] = 1

            v1.mask_coordinates['b1'] = [
                [1, 1], [1, 2], [2, 2], [2, 1], [1, 1]]
            total = sum(v1.sum(None))
            v2 = IM.filter.set_zero(v1, ['b1'])
            set_to_zero = (total - sum(v2.sum(None)))

            assert_equal(set_to_zero, v2.frames * 4 * len(v1.axes),
                         'Problem setting coords to zero')

    def testcreate_group(self):

        nf = 2
        for bf in get_bfs():
            gc.collect()

            buff1 = ReadIM.newBuffer(
                [[0, 3], [5, 0]], frames=nf, image_sub_type=bf)

            if bf > 0:
                cls = IM.VC7
            else:
                cls = IM.IM7

            v1 = cls('v1_{0}'.format(bf), buff=buff1)
            for f in range(v1.frames):
                v1[f] = 1.2**f

            ims = []
            ii = 0
            for i in range(10):
                im = v1.copy()
                ims.append(im)
                for f in range(im.frames):
                    im[f] = ii
                    ii += 1
            im2 = IM.filter.create_group(
                'new group', ims, family=ims[0].family)
            assert_equal(im2.frames, len(ims) * nf)
            for i, f in enumerate(im2):

                assert_((f == i).all())

    def testget_merge_name(self):

        names = [os.path.join('level_1', 'level_2', '{0}'.format(i))
                 for i in range(20)]
        mn = IM.filter.get_merge_name(names)
        assert_equal(mn, 'level_1\\level_2\\0-19')

        mn = IM.filter.get_merge_name(names, 'subfile')
        assert_equal(mn, 'level_1\\level_2\\0-19_subfile')

    def testmerge_wts(self):

        for bf in get_bfs():

            gc.collect()
            buff1 = ReadIM.newBuffer(
                [[0, 1], [1, 0]], 10, 10, frames=2, image_sub_type=bf)
            buff2 = ReadIM.newBuffer(
                [[0, 3], [5, 0]], 20, 20, frames=2, image_sub_type=bf)
            buff3 = ReadIM.newBuffer(
                [[4, 10], [6, 5]], 30, 30, frames=2, image_sub_type=bf)

            if bf > 0:
                cls = IM.VC7
            else:
                cls = IM.IM7

            v1 = cls('v1_{0}'.format(bf), buff=buff1)
            v1[:] = 2
            v2 = cls('v2_{0}'.format(bf), buff=buff2)
            v2[:] = 3
            v3 = cls('v3_{0}'.format(bf), buff=buff3)
            v3[:] = 4
##            v5 = cls('v3_{0}'.format(bf), buff=buff2)
##            v5[:] = 5

            ims = [v1, v2, v3]
            v4 = IM.filter.merge_wts(ims)
##            ims = [v1,v2,v3,v4,v5]
##            v6 = IM.filter.merge_wts(ims)
# del(v4)
# del(v5)

            reps = [repr(v) for v in ims]
            for v in ims:
                del(v)
            del(ims)
            del(v1)
            del(v2)
            del(v3)

            gc.collect()
            for r in reps:
                assert_(r not in IM.IM7._instances,
                        "failed to delete object: {0}".format(r))

    def testfilter(self):

        for f in ReadIM.extra.get_sample_image_filenames():
            v1 = IM.IM7(f)

            v2 = IM.filter.filter(v1, nd_image.gaussian_filter, sigma=0.2)
            v3 = IM.filter.filter(v1, nd_image.sobel)
            v4 = IM.filter.filter(v1, nd_image.prewitt)

            v1.show()
            v2.show()
            v3.show()
            v4.show()

        vecfiles = glob.glob(os.path.join(
            ReadIM.extra.get_sample_folder(), '*.vc7'))
        for f in vecfiles:
            v1 = IM.VC7(f)

            v2 = IM.filter.filter(v1, nd_image.gaussian_filter, sigma=0.2)
            v3 = IM.filter.filter(v1, nd_image.sobel)
            v4 = IM.filter.filter(v1, nd_image.prewitt)

            v1.show()
            v2.show()
            v3.show()
            v4.show()

    def testfilter_median(self):
        for f in ReadIM.extra.get_sample_image_filenames():
            v1 = IM.IM7(f)

            v2 = IM.filter.filter_median(v1)

            v1.show()
            v2.show()

        vecfiles = glob.glob(os.path.join(
            ReadIM.extra.get_sample_folder(), '*.vc7'))
        for f in vecfiles:
            v1 = IM.VC7(f)
            v2 = IM.filter.filter_median(v1)
            v1.show()
            v2.show()

    def testfilter_gaussian(self):
        for f in ReadIM.extra.get_sample_image_filenames():
            v1 = IM.IM7(f)

            v2 = IM.filter.filter_gaussian(v1)

            v1.show()
            v2.show()

        vecfiles = glob.glob(os.path.join(
            ReadIM.extra.get_sample_folder(), '*.vc7'))
        for f in vecfiles:
            v1 = IM.VC7(f)
            v2 = IM.filter.filter_gaussian(v1)
            v1.show()
            v2.show()

    def testfilter_gaussian_blur(self):
        for f in ReadIM.extra.get_sample_image_filenames():
            v1 = IM.IM7(f)

            v2 = IM.filter.filter_gaussian_blur(v1, 3)

            v1.show()
            v2.show()

        vecfiles = glob.glob(os.path.join(
            ReadIM.extra.get_sample_folder(), '*.vc7'))
        for f in vecfiles:
            v1 = IM.VC7(f)
            v2 = IM.filter.filter_gaussian_blur(v1, 3)
            v1.show()
            v2.show()

    def testoverlaps_count(self):
        for bf in get_bfs():

            if bf > 0:
                cls = IM.VC7
            else:
                cls = IM.IM7

            buff1 = ReadIM.newBuffer(
                [[0, 1], [1, 0]], 10, 10, frames=2, image_sub_type=bf)
            buff2 = ReadIM.newBuffer(
                [[0, 3], [5, 0]], 20, 20, frames=2, image_sub_type=bf)
            buff3 = ReadIM.newBuffer(
                [[0.5, 2], [6, 0]], 30, 30, frames=2, image_sub_type=bf)

            v1 = cls('v1', buff=buff1)
            v2 = cls('v2', buff=buff2)
            v3 = cls('v3', buff=buff3)

            v1 = IM.VC7('v1', buff=buff1)
            v2 = IM.VC7('v2', buff=buff2)
            v3 = IM.VC7('v3', buff=buff3)

            ims = [v1, v2, v3]

            cnt = IM.filter.overlaps_count(ims)

            assert_equal(cnt, 3, 'Overlaps should equal 3')

    def testoverlaps_splice_iter(self):
        for bf in get_bfs():
            gc.collect()

            if bf > 0:
                cls = IM.VC7
            else:
                cls = IM.IM7

            buff1 = ReadIM.newBuffer(
                [[0, 1], [1, 0]], 10, 10, frames=2, image_sub_type=bf)
            buff2 = ReadIM.newBuffer(
                [[0, 3], [5, 0]], 20, 20, frames=2, image_sub_type=bf)
            buff3 = ReadIM.newBuffer(
                [[0.5, 2], [6, 0]], 30, 30, frames=2, image_sub_type=bf)

            v1 = cls('v1', buff=buff1)
            v2 = cls('v2', buff=buff2)
            v3 = cls('v3', buff=buff3)

            ims = [v1, v2, v3]

            for im in ims:
                im.variables.store(phase_f=np.linspace(0, 1, im.frames))

            cnt = 0
            for im in IM.filter.overlaps_splice_iter(ims):
                cnt += 1

            assert_equal(cnt, 3, 'Overlaps should equal 3')


if __name__ == '__main__':
    test = TestFilters('testmerge_wts')
    test.debug()
# run_module_suite()
